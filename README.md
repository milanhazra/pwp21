# PWP21

PWP

# Description of the Project

To capture the plastic and litter footprint of commodity manufacturers based on garbage analysis


# Detailed Description
# Introduction/Background
Swach Bharat Mission
The overall objective of the mission is to provide complete sanitation solutions for all of India’s 4041 statutory towns.  India’s second most populous state, Maharashtra has become the 18th state in the country to ban plastic after the government issued the Maharashtra Plastic and Thermocol Products (Manufacture, Usage, Sale, Transport, Handling and Storage) Notification, 2018 on March 23. This ban has come within six months after State Environment Minister Ramdas Kadam took up this issue with the cabinet.
With this view, this season of Project Deep Blue has chosen the theme of sanitation and public wellbeing.
While most of these initiatives are focused on changing behavior of individuals, this problem statement takes once step further to associate responsibility for environment and cleanliness to the large manufactures who put these products on the market.
Problem Statement

This problem statement looks at creating a citizen app which can capture and contribute pictures  of garbage dumped at a collection point and litter . A back end algorithm can then  analyze the brands of packaging and track and identify foot print of the manufacturers.
Problem Dimensions

This problem has the following dimensions

Capturing the images of the garbage dumps or collections points. The algorithm should be able to, based on the image, identify the discarded packaging of common commodity items like chips, biscuits, cold drinks etc.
Based on the identified brand, over a duration or over a set of locations be able generate a plastic and litter profile of the manufacturer
For example, the common litter elements include wrappers of chocolates, packets of kurkure or lays or empty bottles of cold drinks which are being used and the packaging being disposed off. By analyzing these products, the algorithm should be able to create a plastic waste and litter profile of different brands.

So That

The objective of the solution should be able to come up an algorithm which can create plastic and litter profile of commodity brands. Social media could then be the next logical step to share this information to ensure that large manufacturers are also accountable for environmental impact of their products

#Actors

Citizen

-MileStone1
--Denoising->Detecting Plastic->Cropping Images->Color and TextExtraction

## Checkpoint 1 for palstic non wrappers
!![GitLab Logo](/DeepBlue_1/Pwp-web/Web/demo/testpwp/static/image/image_plastic/image1.jpg)


## Checkpoint 2 for palstic, brands!!
!![GitLab Logo](/DeepBlue_1/Pwp-web/Web/demo/testpwp/static/image/image_brand/image1.jpg)