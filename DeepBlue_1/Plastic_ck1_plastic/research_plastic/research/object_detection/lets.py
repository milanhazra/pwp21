import firebase_admin
from firebase_admin import credentials
from firebase_admin import storage
import datetime
import urllib.request


def image_download(url, name_img) :
    urllib.request.urlretrieve(url, name_img)

cred = credentials.Certificate("credentials.json")

# Initialize the app with a service account, granting admin privileges
app = firebase_admin.initialize_app(cred, {
    'storageBucket': 'testing-b11c1.appspot.com',
})
url_img = "gs://testing-b11c1.appspot.com/"
bucket_1 = storage.bucket(app=app)
image_urls = []

for blob in bucket_1.list_blobs():
	name = str(blob.name)
	#print(name)
	blob_img = bucket_1.blob(name)
	X_url = blob_img.generate_signed_url(datetime.timedelta(seconds = 300), method='GET')
	#print(X_url)
	image_urls.append(X_url)


PATH = ['/home/milan/Desktop/pwp21/DeepBlue_1/Plastic_ck1_plastic/research_plastic/research/object_detection/test_images/','/home/milan/Desktop/pwp21/DeepBlue_1/Plastic_ck2_brand/research_brand/research/object_detection/test_images/']
for path in PATH:
	i = 1
	for url  in image_urls:
		name_img = str(path + "image"+str(i)+".jpg")
		image_download(url, name_img)
		i+=1