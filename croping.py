import cv2 
from PIL import Image
import os

def image_resize(image, inter = cv2.INTER_AREA):
	width = 150
	height = 150
	(h, w) = image.shape[:2]
	if width is None and height is None:
		return image
	# check to see if the width is None
	elif width is None:
		r = height / float(h)
		dim = (int(w * r), height)
	else:
		r = width / float(w)
		dim = (width, int(h * r))

	resized = cv2.resize(image, dim, interpolation = inter)
    # return the resized image
	return resized



image = cv2.imread("images.jpeg")

# Recolour Image
gray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

# Edge Detection
edged = cv2.Canny(image, 100, 250)

# Finding and Separating Contours
_,cnts,_ = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
idx = 0
path = '/home/milan/Desktop/croped_images'
for c in cnts:
    x,y,w,h = cv2.boundingRect(c)
    if w>50 and h>50:
        idx+=1
        new_img=image[y:y+h,x:x+w]
        resize_img = image_resize(new_img)
        cv2.imwrite("Image "+ str(idx) + '.jpeg', resize_img)
        cv2.imwrite(os.path.join(path,"Image " + str(idx) + '.jpeg'), resize_img)

cv2.waitKey(1)
cv2.destroyAllWindows()
